defmodule HelpScoutElixir do
  alias HelpScoutElixir.Producer
  alias HelpScoutElixir.Endpoints

  @moduledoc """
  Documentation for HelpScoutElixir.
  """

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      @otp_app Keyword.fetch!(opts, :otp_app)
      @config build_config()

      #:async false?
      def get_mailbox(id, opts \\ []) do
        tag = UUID.uuid1()

        #Dynamic Producer and Consumer?
        # {:via, DynamicSupervisor, {:id, Consumer, "get_mailbox"}}
        unless GenServer.whereis(GetMailboxConsumer) do
          DynamicSupervisor.start_child(
            Consumer,
            {Consumer, [id: GetMailboxConsumer, name: GetMailboxConsumer start: {Consumer, :start_link, [subscribe_slug: "get_mailbox"]}]}
          )
        end

        Producer.cast(
          Producer,
          {
            "get_mailbox",
            {&Mailbox.get_mailbox/1, [config: @config, id: id], tag}
          }
        )

        ResultQueue.get(tag)

        # Mailbox.request(&Mailbox.get_mailbox/2, [config: @config, id: id])
      end

      def list_mailbox_custom_fields(id, opts \\ []) do
        # tag = UUID.uuid1()

        Producer.cast(
          {&Mailbox.list_mailbox_custom_fields/1, [config: @config, id: id]}
        )
      end

      def list_mailbox_folders(id, opts \\ []) do
        # tag = UUID.uuid1()

        Producer.cast(
          {&Mailbox.list_mailbox_folders/1, [config: @config, id: id]}
        )
      end

      defp build_config, do: __MODULE__.build_config(@otp_app)

      # defp result(tag) do

      # end
    end
  end

  def build_config(otp_app) do
    env = Application.fetch_env!(otp_app, __MODULE__)

    %{
      client_id: Keyword.fetch!(env, :client_id)
      client_secret: Keyword.fetch!(env, :client_secret)
    }
  end
end

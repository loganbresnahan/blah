defmodule HelpScoutElixir.RequestState do
  use Agent

  def start_link(_) do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def get_state do
    Agent.get(__MODULE__, fn(state) -> state end)
  end
end

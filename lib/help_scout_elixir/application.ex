defmodule HelpScoutElixir.Application do
  use Application

  def start(_type, _args) do
    children = [
      HelpScoutElixir.RequestState.child_spec(id: HelpScoutElixir.RequestState),
      HelpScoutElixir.DynamicSupervisor.child_spec(id: HelpScoutElixir.DynamicSupervisor)
      # HelpScoutElixir.Producer.child_spec(id: HelpScoutElixir.Producer),
      # HelpScoutElixir.Consumer.child_spec(id: HelpScoutElixir.Consumer)
    ]

    Supervisor.start_link(children, [strategy: :rest_for_one])
  end
end

defmodule HelpScoutElixir.Consumer do
  use GenStage

  def start_link(opts) do
    GenStage.start_link(__MODULE__, opts)
  end

  def init([subscribe_slug: subscribe_slug]) do
    state = %{producer: HelpScoutElixir.Producer}

    GenStage.sync_subscribe(
      __MODULE__,
      to: state.producer,
      selector: fn({slug, _}) -> slug == subscribe_slug end
    )

    {:consumer, state, [state.producer]}
  end

  def handle_info(_, state) do
    {:noreply, [], state}
  end

  def handle_event(event, from, state) do
    {_, fun, args} = event
    event_result = fun.(args)

    Process.send(from, {:update, event_result})

    receive do
      {{:update, _}, ^from, _, _, _} ->
        {:noreply, [], state}
    after
      5000 ->
        {:noreply, [], state}
    end
  end

  # def child_spec(opts) do
  #   %{
  #     id: Keyword.fetch!(opts, :id)
  #     subscription: Keyword.fetch!(opts, :subscription)
  #   }
  # end
end

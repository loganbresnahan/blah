defmodule HelpScoutElixir.Endpoints.Mailbox do
  alias HelpScoutElixir.HTTPClient

  def request(fun, args) do
    fun.(args)
    |> ResponseHandler.process()
  end

  def get_mailbox([config: config, id: id], access_token) do
    "mailboxes/"<>id<>"?access_token="<>access_token
    |> Client.get(config)
  end

  def list_mailbox_custom_fields([config: config, id: id], access_token) do
    "mailboxes/"<>id<>"/fields?access_token="<>access_token
    |> Client.get(config)
  end

  def list_mailbox_folders([config: config, id: id], access_token) do
    "mailboxes/"<>id<>"/folders?access_token="<>access_token
    |> Client.get(config)
  end
end

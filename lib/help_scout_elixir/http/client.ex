defmodule HelpScoutElixir.HTTP.Client do
  @host "https://api.helpscout.net/v2"

  def get(path, config, [grant_type: grant_type]) do
    access_token = TokenAgent.get()

    HTTPoison.get(
      @host<>path<>"?access_token="<>access_token
    )
  end
end

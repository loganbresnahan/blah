defmodule HelpScoutElixir.Producer do
  use GenStage

  def start_link(_args) do
    GenStage.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    {:producer, [], dispatcher: GenStage.BroadcastDispatcher}
  end

  # Public API
  def cast(event) do
    GenServer.cast(__MODULE__, {:add, event})
  end

  def get(tag) do

  end

  # Private Callbacks
  def handle_cast({:add, event}, state) do
    {:noreply, event, [event] ++ state}
  end

  def handle_info({:update, {:ok, event}}, state) do

    {:noreply, [], state -- [event]}
  end

  def handle_info({:update, {:error, event}}, state) do

    {:noreply, [], [event] ++ state}
  end

  def handle_demand(_, state) do
    {:noreply, [], state}
  end

  defp update_event_state do

  end
end

defmodule HelpScoutElixir.MixProject do
  use Mix.Project

  def project do
    [
      app: :help_scout_elixir,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {HelpScoutElixir.Application, []},
      extra_applications: [:logger, :httpoison]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.0"},
      {:gen_stage, "~> 0.14"},
      {:elixir_uuid, "~> 1.2"}
    ]
  end
end
